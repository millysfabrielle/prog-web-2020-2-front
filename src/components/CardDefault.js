
import React from 'react';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import {Link} from "react-router-dom";

class CardDefault extends React.Component{
    render(){
        return(
            <div>
                <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={this.props.img} />
                    <Card.Body>
                    <Card.Title>{this.props.title}</Card.Title>
                    <Card.Text>{this.props.text}</Card.Text>
                        <Link to={this.props.link}>Detalhes</Link>
                    </Card.Body>
                  </Card>

            </div>
        );
    }
}

export default CardDefault;
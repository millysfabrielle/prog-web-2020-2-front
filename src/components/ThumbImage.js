import React, { Component } from 'react';

class ThumbImage extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            thumb: undefined,
          };

    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.file) { return; }

        this.setState({ loading: true }, () => {
            
            if(typeof nextProps.file == "object"){
                let reader = new FileReader();
                reader.onloadend = () => {
                    this.setState({ loading: false, thumb: reader.result });
                };
                reader.readAsDataURL(nextProps.file);
            } else {
                this.setState({ loading: false, thumb: nextProps.file });
            }


            
            
        });


    }

    render() {
        const { file } = this.props;
        const { loading, thumb } = this.state;
    
        if (!file) { return null; }
    
        if (loading) { return <p>loading...</p>; }
    
        return (<img src={thumb}
          alt={file.name}
          className="img-thumbnail mt-2"
          height={100}
          width={100} />);
      }


}

export default ThumbImage;
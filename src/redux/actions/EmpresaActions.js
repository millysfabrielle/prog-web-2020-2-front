import EmpresaService from "../../services/EmpresaService";

export const EMPRESA_ACTIONS = {
    LISTAR: "EMPRESA_LISTAR",
    BUSCAR: "EMPRESA_BUSCAR",
    SALVAR: "EMPRESA_SALVAR",
    ALTERAR: "EMPRESA_ALTERAR",
    EXCLUIR: "EMPRESA_EXCLUIR"
}

export function salvarEmpresa(empresa){
    return function (dispatch){
        return EmpresaService.salvar(empresa)
        .then( response => {

            if(empresa.id){
                dispatch({
                    type: EMPRESA_ACTIONS.ALTERAR,
                    content: response.data
                })
            } else {
                dispatch({
                    type: EMPRESA_ACTIONS.SALVAR,
                    content: response.data
                })
            }

        }) 
        .catch(error => {
            console.log(error);
        })       
    }
}

export function excluirEmpresa(id){
    return function (dispatch){

        return EmpresaService.excluir(id)
        .then( response => {
            dispatch({
                type: EMPRESA_ACTIONS.EXCLUIR,
                content: id // ID que foi excluido
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
}

export function buscarEmpresa(id){
    return function(dispatch){
        return EmpresaService.buscar(id)
        .then( response => {
            dispatch({
                type: EMPRESA_ACTIONS.BUSCAR,
                content: response.data
            });
        })
        .catch(error => {
            console.log(error);
        })
    }
}

export function listarEmpresa(){
    return function (dispatch){

        return EmpresaService.listar()
        .then( response => {

            dispatch({
                type: EMPRESA_ACTIONS.LISTAR,
                content: response.data
            })

        })
        .catch(error => {
            console.log(error);
        })

    }
}
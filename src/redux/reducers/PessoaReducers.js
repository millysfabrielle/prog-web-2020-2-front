import {PESSOA_ACTIONS} from "../actions/PessoaActions";

const pessoaState = {
    pessoaLista: [],
    pessoaItem: {}
}

export default function pessoaReducer( state = pessoaState, dispatch ){

    switch(dispatch.type){

        case PESSOA_ACTIONS.LISTAR:
            return {
                ...state, /* SPREAD */
                pessoaLista: dispatch.content
            }

        case PESSOA_ACTIONS.EXCLUIR:
            return {
                ...state,
                pessoaLista: state.pessoaLista.filter( function(item){
                    return item.id != dispatch.content
                })
            }

        case PESSOA_ACTIONS.SALVAR:
            return {
                ...state,
                pessoaLista: state.pessoaLista.concat(dispatch.content),
                pessoaItem: dispatch.content,
            }

        case PESSOA_ACTIONS.ALTERAR:
            return {
                ...state,
                pessoaLista: state.pessoaLista.map( pessoa => {
                    if(pessoa.id == dispatch.content.id){
                        return dispatch.content;
                    }
                    return pessoa;
                }),
                pessoaItem: dispatch.content
            }

        case PESSOA_ACTIONS.BUSCAR:
            return {
                ...state,
                pessoaItem: dispatch.content
            }
        
        default:
            return state;


    }

}
import axios from 'axios';

class EmpresaService{

    constructor(){
        this.connection = axios.create({baseURL: 'https://mighty-chamber-65327.herokuapp.com'});
    }

    listar(){
        return this.connection.get('/empresa');
    }

    buscar(id){
        return this.connection.get('/empresa/'+id);
    }

    salvar(empresa){
        if(empresa.id){
            return this.connection.put('/empresa', empresa);
        }
        return this.connection.post('/empresa', empresa);
    }

    excluir(id){
        return this.connection.delete('/empresa/'+id);
    }

}

export default new EmpresaService();
import axios from 'axios';

class PessoaService{

    constructor(){
        //this.connection = axios.create({baseURL: 'https://mighty-chamber-65327.herokuapp.com'});
        this.connection = axios.create({baseURL: 'http://localhost:8000'});
    }

    listar(){
        return this.connection.get('/pessoa');
    }

    buscar(id){
        return this.connection.get('/pessoa/'+id);
    }

    salvar(pessoa){
        if(pessoa.id){
            return this.connection.put('/pessoa', pessoa);
        }
        return this.connection.post('/pessoa', pessoa);
    }

    excluir(id){
        return this.connection.delete('/pessoa/'+id);
    }

}

export default new PessoaService();
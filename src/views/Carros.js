import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CardDefault from '../components/CardDefault';

class Carros extends React.Component{

    constructor(props){
        super(props) 

    }


    render(){
        return(
            <div>

                <h1 style={{backgroundColor: "green", color: "blue"}} >CARROS</h1>
                {parseInt(this.props.x)  + 6}
                <Row>
                    <Col> <CardDefault img={`img/${this.props.img}.jpeg`} title="Accord" text="Texto do Accord!" link="/carro-detalhes/accord" ></CardDefault> </Col>
                    <Col> <CardDefault img="img/crv.jpeg" title="CRV" text="Texto do CRV!" link="/carro-detalhes/crv" ></CardDefault> </Col>
                    <Col> <CardDefault img="img/honda.jpeg" title="Civic" text="Texto do Civic!" link="/carro-detalhes/civic" ></CardDefault> </Col>
                </Row>


            </div>
        );
    }
}

export default Carros;
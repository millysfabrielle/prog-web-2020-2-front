import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage, } from 'formik';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

import {salvarEmpresa, buscarEmpresa} from "../../redux/actions/EmpresaActions";
import {empresaItemSelector} from "../../redux/selectors/EmpresaSelectors";

var initialValues = {
    nome: "",
    cnpj: "",
    endereco: ""
}

class EmpresaForm extends React.Component{

    constructor(props){
        super(props);

        const { id } = this.props.match.params;

        if(id){
            this.props.buscarEmpresa(id);
        }
    }

    handleSubmit = (values, {setSubmitting}, props) => {

        this.props.salvarEmpresa(values);
        setSubmitting(false);
        this.props.history.push('/empresa/list');
    }


    render() {

        return(

            <div>
                
                <h1>Formulário de empresa</h1>

                <Formik
                    initialValues= { this.props.match.params.id ? this.props.empresaItem : initialValues }
                    enableReinitialize
                    onSubmit={ (values, actions) =>  this.handleSubmit(values, actions, this.props)  }
                >

                    { ({values, handleSubmit, isSubmitting}) => (

                        <Form onSubmit={handleSubmit}>

                            <div>
                                <label>Nome:</label>
                                <Field type="text" name="nome" />
                                <ErrorMessage name="nome" component="div"/>
                            </div>

                            <div>
                                <label>CNPJ:</label>
                                <Field type="text" name="cnpj" />
                                <ErrorMessage name="cnpj" component="div"/>
                            </div>

                            <div>
                                <label>Endereço:</label>
                                <Field type="text" name="endereco" />
                                <ErrorMessage name="endereco" component="div"/>
                            </div>

                            <div>
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                                <Link to="/empresa/list" className="btn btn-danger">Cancelar</Link>
                            </div>

                        </Form>
                    )}

                    
                </Formik>

            </div>

        );

    }

}

export default connect(empresaItemSelector,{salvarEmpresa, buscarEmpresa})(EmpresaForm);
import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage, } from 'formik';
import { Link } from 'react-router-dom';

import ThumbImage from "../../components/ThumbImage";

import { connect } from "react-redux";
import {salvarPessoa, buscarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaItemSelector} from "../../redux/selectors/PessoaSelectors";

var initialValues = {
    nome: "",
    imagem: null
}

class PessoaForm extends React.Component{

    constructor(props){
        super(props);
        const { id } = this.props.match.params;

        if(id){
            this.props.buscarPessoa(id);
        }

    }

    handleSubmit =  (values, {setSubmitting}, props) => {
        
        if(!values.imagem){
            this.props.salvarPessoa(values);
            setSubmitting(false);
            this.props.history.push('/pessoa/list');
        } else {

            this.convertBase64(values.imagem)
                .then(reponse => {

                    let data = values;
                    data.imagem = reponse;

                    this.props.salvarPessoa(values);
                    setSubmitting(false);
                    this.props.history.push('/pessoa/list');

                })
                .catch(error => {
                    console.log("ERRO: ", error)
                });

        }

        


    }

    convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.readAsDataURL(file);
    
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
    
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
      };

    render() {

        return(

            <div>
                
                <h1>Formulário de pessoa</h1>

                <Formik
                    initialValues= { this.props.match.params.id ? this.props.pessoaItem : initialValues }
                    enableReinitialize
                    onSubmit={ (values, actions) =>  this.handleSubmit(values, actions, this.props)  }
                >

                    { ({values, handleSubmit, isSubmitting, setFieldValue}) => (

                        <Form onSubmit={handleSubmit}>

                            <div>
                                <label>Imagem de perfil:</label>
                                <input id="imagem" name="imagem" type="file" onChange={(event) => { 
                                        setFieldValue("imagem", event.currentTarget.files[0]);
                                    }} className="form-control" />
                                <ThumbImage file={values.imagem} />

                            </div>

                            <div>
                                <label>Nome:</label>
                                <Field type="text" name="nome" />
                                <ErrorMessage name="marca" component="div"/>
                            </div>

                            <div>
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                                <Link to="/pessoa/list" className="btn btn-danger">Cancelar</Link>
                            </div>
                            
                            {JSON.stringify(values)}

                        </Form>

                    )}

                    
                </Formik>

            </div>

        );

    }

}

export default connect(pessoaItemSelector,{salvarPessoa, buscarPessoa})(PessoaForm);
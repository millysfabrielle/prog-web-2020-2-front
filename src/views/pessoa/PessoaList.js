import React, { Component } from 'react';
import { connect } from "react-redux";

import './Pessoa.css';

import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import { Link } from 'react-router-dom';

import {listarPessoa, excluirPessoa} from '../../redux/actions/PessoaActions';
import {pessoaListaSelector} from '../../redux/selectors/PessoaSelectors';

class PessoaList extends React.Component{ 
    
    constructor(props, context){
        super(props, context);

        this.props.listarPessoa();

        this.state = {
            search: "",
            modalShow: false,
            pessoa: {}
        }
    }

    handleExcluir = () => {
        this.props.excluirPessoa(this.state.pessoa.id)
        .then( () => {
            this.handleClose()
        })
        .catch();
        
    }

    handleClose = () =>{
        this.setState({modalShow: false});
    }

    handleShow = (pessoaItem) => {
        this.setState({modalShow: true});
        this.setState({pessoa: pessoaItem});
        
    }

    handleSearch = (event) => {
        this.setState({search: event.target.value});
    }

    render(){
        return(
            <div>

                <h1>Lista de pessoas</h1>
                
                    <InputGroup className="mb-3">
                    <Form.Control type="search" onChange={this.handleSearch} placeholder="Pesquisar pelo nome" />
                    <InputGroup.Append>
                        <InputGroup.Text id="basic-addon1">Filtar</InputGroup.Text>
                    </InputGroup.Append>
                    </InputGroup>

                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Thumb</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Operações</th>
                        </tr>
                    </thead>
                    <tbody>

                        { this.props.pessoaLista
                        .filter( pessoa => pessoa.nome.toLowerCase().includes(this.state.search.toLowerCase()) )
                        .map( (pessoa,index) => 
                            
                            <tr key={index}>
                                <td className="thumbnail">
                                    <img src={pessoa.imagem} />
                                </td>
                                <td>{pessoa.nome}</td>
                                <td>
                                <Link to={`/pessoa/form/${pessoa.id}`} className="btn btn-primary">Alterar</Link>
                                     | 
                                <Button variant="primary" onClick={() => this.handleShow(pessoa)}>
                                    Excluir
                                </Button>
                                </td>
                            </tr>    
                              
                        )}


                    </tbody>
                </table>
                
                <Link to="/pessoa/form" className="btn btn-primary">Novo</Link>

                
                <Modal show={this.state.modalShow} onHide={this.handleClose} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Deseja excluir a pessoa {this.state.pessoa.nome}</Modal.Title>
                    </Modal.Header>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="danger" onClick={this.handleExcluir}>
                        Excluir
                    </Button>
                    </Modal.Footer>
                </Modal>


            </div>
        );
    }
}

export default connect(pessoaListaSelector, {listarPessoa, excluirPessoa})(PessoaList);